#include "BTree.h"

void freePage(btPage *page){
    assert(page != NULL);
    free(page->records);
    free(page->childs);
    free(page);
}

btPage *readPageFromFile(FILE *fp){
    //Aloca espaço para carregar página
    btPage *item = (btPage *) calloc(1, sizeof(btPage));
    item->childs = (long *) calloc(MAXKEYS + 1, sizeof(long));
    item->records = (record *) calloc(MAXKEYS, sizeof(record));

    //Lê dados da página do arquivo
    int i = 0;
    for (i = 0; i < MAXKEYS; i++)
    {
        fread(&item->records[i].key, sizeof(int), 1, fp);
        fread(&item->records[i].recordRRN, sizeof(long), 1, fp);
    }

    fread(item->childs, sizeof(long), MAXKEYS + 1, fp);
    fread(&item->numberOfKeys, sizeof(short), 1, fp);
    fread(&item->isLeaf, sizeof(bool), 1, fp);

    //Retorna a pagina lida
    return item;
    
}


int writePageIntoFile(long rrn, btPage *page, FILE *fp){
    //Verifica se está tudo ok com os dados
    assert(fp != NULL);
    assert(page != NULL);

    //Encontra local para escrita baseado no RRN
    fseek(fp, rrn * PAGESIZE, SEEK_SET);
    
    //Escreve dados
    for (int i = 0; i < MAXKEYS; i++) {
        fwrite(&page->records[i].key, sizeof(int), 1, fp);
        fwrite(&page->records[i].recordRRN, sizeof(long), 1, fp);
    }

    fwrite(page->childs, sizeof(long), MAXKEYS + 1, fp);
    fwrite(&page->numberOfKeys, sizeof(short), 1, fp);
    fwrite(&page->isLeaf, sizeof(bool), 1, fp);

    //Atualiza valor de espaço livre na página
    unsigned char *padding = (unsigned char *) calloc(FREE_SPACE_ON_PAGE, sizeof(unsigned char));
    fwrite(padding, sizeof(unsigned char), FREE_SPACE_ON_PAGE, fp);
    free(padding);

    fflush(fp);

    return 0;
}

long getNextRRNToWriteInFile(FILE *fp){
    fseek(fp, 0, SEEK_END);
    return ftell(fp)/PAGESIZE;
}

btPage *getPage(long RRN, FILE *fp){
    //Recupera uma página baseado no RRN
    //Procura e carrega seus dados
    assert(fp != NULL);
    fseek(fp, RRN * PAGESIZE, SEEK_SET);
    
    btPage *page = readPageFromFile(fp);
    page->pageRRN = RRN;
    return page;

}

long getTreeHeader(FILE *fp){
    //Carrega o cabeçalho da árvore, que está no início do arquivo
    assert(fp != NULL);
    fseek(fp, 0, SEEK_SET);

    long rootPosition;
    fread(&rootPosition, sizeof(long), 1, fp);
    return rootPosition;

}

void writeTreeHeader(FILE *fp, long rootRRN){
    //Calcula espaço livre e escreve no cabeçalho da árvore, junto com o RRN do nó raíz
    assert(fp != NULL);
    fseek(fp, 0, SEEK_SET);
    unsigned char *padding = (unsigned char *)calloc(PAGESIZE-sizeof(long), sizeof(unsigned char));


    fwrite(&rootRRN, sizeof(long), 1, fp);
    fwrite(padding, sizeof(unsigned char), PAGESIZE-sizeof(long), fp);
    fflush(fp);
    free(padding);
}

btPage *createTree(FILE *fp){
    assert(fp != NULL);
    //Aloca espaço pra raiz
    btPage *root = (btPage *) calloc(1, sizeof(btPage));
    root->childs = (long *) calloc(MAXKEYS + 1, sizeof(long));
    root->records = (record *) calloc(MAXKEYS, sizeof(record));
    
    //Inicializa os valores
    root->isLeaf = true;
    root->numberOfKeys = 0;
    root->pageRRN = 1;

    for (int i = 0; i < MAXKEYS; i++) {
        root->records[i].recordRRN = -1;        
    }
    for (int i = 0; i < MAXKEYS + 1; i++) {
        root->childs[i] = -1;        
    }
    
    //Escreve a raiz no cabeçalho
    writeTreeHeader(fp, 1);
    return root;
}

// determina a existencia da arvore
long doesTreeExists(FILE *fp){
    assert(fp != NULL);
    fseek(fp, 0, SEEK_END);
    return ftell(fp);
}

btPage *getOrCreateRoot(FILE *fp){
    //Verifica se a árvore já existe ou precisa criar uma nova
    
    //  Se a árvore não existir, cria ela
    if (!doesTreeExists(fp)) {
        return createTree(fp);
    }
    
    // Se existir, só pega o RRN da raiz no cabeçalho e carrega sua página
    else {
        long initialRRN = getTreeHeader(fp);
        return getPage(initialRRN, fp);
    }
}


promotedKey *extractpromotedKey(btPage *page){
    //Aloca espaço pra chave
    promotedKey * newKey = (promotedKey *) calloc(1, sizeof(promotedKey));

    // atribui os valores do registro de indice
    newKey->key = page->records[0].key;
    newKey->recordRRN = page->records[0].recordRRN;

    //Atualiza dados da página (filho, número de chaves, etc)

    //faz o shift dos elementos já existentes na pagina
    memmove(page->records, &page->records[1], (page->numberOfKeys - 1)*sizeof(record));
    // seta o ultimo elemento como inexisten
    memset(&page->records[page->numberOfKeys-1], -1, sizeof(record));

    // remove o filho a esqueda
    memmove(page->childs, &page->childs[1], (page->numberOfKeys)*sizeof(long));
    // seta o ultimo elemento como inexistente
    memset(&page->childs[page->numberOfKeys], -1, sizeof(long));



    // atualiza a quantidade de chaves na pagina
    page->numberOfKeys--;
    
    return newKey;

}

btPage *createNewNodePage(btPage *leftPage){
    // alocação do novo nó
    btPage *rightPage = (btPage *) calloc(1, sizeof(btPage));
    rightPage->childs = (long *) calloc(MAXKEYS + 1, sizeof(long));
    rightPage->records = (record *) calloc(MAXKEYS, sizeof(record));
    memset(rightPage->childs, -1, sizeof(long) * (MAXKEYS + 1));

    // seu tipo sera o mesma do no que o gerou... 
    rightPage->isLeaf = (leftPage->isLeaf);

    // splitPoint nos indica onde devemos dividir o no
    short splitPoint = (leftPage->numberOfKeys / 2);


    // trabalhando com os registros

    // retirando os registros
    memmove(rightPage->records, &leftPage->records[splitPoint], (leftPage->numberOfKeys - splitPoint)*sizeof(record));
    // seta os elementos como inexistentes
    memset(&leftPage->records[splitPoint], -1, (leftPage->numberOfKeys - splitPoint)*sizeof(record));


    // trabalhando com os filhos

    // divindo também os filhos
    memmove(rightPage->childs, &leftPage->childs[splitPoint], (leftPage->numberOfKeys - splitPoint + 1)*sizeof(long));
    // seta os elementos como inexistentes
    memset(&leftPage->childs[splitPoint+ 1], -1, (MAXKEYS - splitPoint)*sizeof(long));

    // podemos determinar o numero de chaves
    rightPage->numberOfKeys = leftPage->numberOfKeys - splitPoint;
    // com a reducao atualizar a quantidade de chaves
    leftPage->numberOfKeys = splitPoint;

    // retorna o novo nó, além de já ter realizado as devidas atualizações no nó que o gerou
    return rightPage;
}

promotedKey *_split(btPage *leftPage,FILE *fp, promotedKey *newKey){
    //Divide a página, cria o novo nó
    btPage *rightPage = createNewNodePage(leftPage);    
    
    //Extrai a chave promovida e atualiza os filhos da chave
    // promokey == o menor filho dos maiores elementos
    promotedKey *promoKey = extractpromotedKey(rightPage);

    //Escreve a página nova e a que foi dividida (com suas atualizações) no arquivo

    // determina o filho a esquerda (pagina já existente)
    promoKey->childs[0] = leftPage->pageRRN;
    // determina o filho a direita (nova pagina, recem criada)
    rightPage->pageRRN = getNextRRNToWriteInFile(fp);
    promoKey->childs[1] = rightPage->pageRRN;

    // reescreve a pagina a esquerda no seu devido lugar
    writePageIntoFile(promoKey->childs[0], leftPage, fp);
    // escreve nova pagina ao final do arquivo
    writePageIntoFile(promoKey->childs[1], rightPage, fp);

    freePage(rightPage);


    return promoKey;
}


btPage *searchPositionOnPageAndInsert(btPage *page, promotedKey *newKey){
    //Encontra a posição para inserir a chave
    short expectedIndex = 0;

    while (expectedIndex < page->numberOfKeys && page->records[expectedIndex].key <  newKey->key) {
    // enquanto não estivermos proximo o suficiente, percorre os valores até encontrar a posicao adequada
        expectedIndex++;
    }

    // a partir daqui temos um a posição onde devemos inserir

    // preparo para insercao
    // manipulação de registros

    memmove(&page->records[expectedIndex+1], &page->records[expectedIndex], (page->numberOfKeys - expectedIndex)*sizeof(record));
    
    // manipulação dos espaços para filhos
    memmove(&page->childs[expectedIndex+1],&page->childs[expectedIndex], (page->numberOfKeys - expectedIndex + 1)*sizeof(long));
    
    memmove(&page->childs[expectedIndex], newKey->childs, 2*sizeof(long));
    
    // inserção do no
    page->records[expectedIndex].key = newKey->key;
    page->records[expectedIndex].recordRRN = newKey->recordRRN;
    // incremento da qtd de chaves
    page->numberOfKeys++;


    return (page);
}

promotedKey * insertIntoNode(btPage *page, promotedKey *newKey, FILE *fp){
    //Procura local pra inserir nova chave na página
    page = searchPositionOnPageAndInsert(page, newKey);

    //Se não couber, splitta ele
    if (page->numberOfKeys >= MAXKEYS) {
        return _split(page, fp, newKey);
    }
    
    //Escreve dados na página
    writePageIntoFile(page->pageRRN,page,fp);
    return NULL;

}

btPage *createNodeWithPromotedKey(promotedKey *promoKey){
    //Se promoção cria estrutura para nova raiz,
    //Aloca espaço para ela
    btPage *newRoot = (btPage *) calloc(1,sizeof(btPage));
    newRoot->childs = (long *) calloc(MAXKEYS + 1, sizeof(long));
    newRoot->records = (record *) calloc(MAXKEYS, sizeof(record));
    
    // a raiz nunca é folha
    newRoot->isLeaf = false;
    memset(newRoot->childs, -1, sizeof(long)*(MAXKEYS+1));

    // salvando a chave
    newRoot->records[0].key = promoKey->key;
    newRoot->records[0].recordRRN = promoKey->recordRRN;
    
    // salvando os filhos (esquerdo e direito)
    newRoot->childs[0] = promoKey->childs[0];
    newRoot->childs[1] = promoKey->childs[1];
    // incrementando a qtd de chaves
    newRoot->numberOfKeys++;

    return newRoot;
}

btPage * setNodeAsRoot(btPage *page, FILE *fp){
    //Escreve página nova e atualiza o cabeçalho para conter ela como raiz
    //Deveria ser chamada junto com criação de novo nó quando promoção cria uma nova raiz
    
    // recebe o rrn da nova pagina ser raiz
    page->pageRRN = getNextRRNToWriteInFile(fp);
    
    // escreve no header
    writeTreeHeader(fp, page->pageRRN);

    // escreve no arquivo
    writePageIntoFile(page->pageRRN, page, fp);


    // TODO return sucess
    return page;
}

promotedKey *_bTreeInsert(btPage *page, promotedKey *key, FILE *fp){
    
    promotedKey *promoKey = NULL;

    //Se nó a ser inserido a chave é folha, tenta inserir
    //Caso a inserção crie uma promoção, precisa retornar a chave promovida para a recursão
    if (page->isLeaf) {
        promoKey = insertIntoNode(page, key, fp);
        return promoKey;
    }
    else {
        //Se não for nó folha, procura qual sub-árvore seguir para inserir numa folha
        short expectedIndex = 0;

        while (expectedIndex < page->numberOfKeys && page->records[expectedIndex].key <  key->key) {
        // enquanto não estivermos proximo o suficiente, percorre os valores até encontrar a posicao adequada
            expectedIndex++;
        }
        // se chegarmos ao final, reduz-se um elemento (respeitar o maxkeys)
        if (expectedIndex == page->numberOfKeys) {
            expectedIndex--;
        }
        

        //Se não for nó folha, procura qual sub-árvore seguir para inserir numa folha
        //Encontrar a posição correta e descer para filho à esquerda se a chave for menor
        //E descer à direita se for maior
        btPage *newPage = NULL;

        if (key->key < page->records[expectedIndex].key) {
            /* a esquerda */
            newPage = getPage (page->childs[expectedIndex], fp);
        }
        else {
            /* a direita */
            newPage = getPage (page->childs[expectedIndex+1], fp);
        }

        //Chamar a inserção recursiva pro filho escolhido
        promoKey = _bTreeInsert(newPage, key, fp);

        promotedKey *resPromoKey = NULL;
        //Se a inserção recursiva retornar uma chave promovida, precisa tentar inserir essa chave promovida no nó atual
        //Retornar chave promovida ou um valor NULL se não houve promoção
        if (promoKey != NULL) {
            resPromoKey = insertIntoNode(page, promoKey, fp);
            free(promoKey);
        }
        
        freePage(newPage);

        return resPromoKey;
    }    

}
//Função mais abstrata de inserção
//Prepara os dados da nova chave
//Tenta inserir recursivamente
//Se tiver chave promovida no final da recursão, é que existe nova raiz
//Chama as funções pra criar nova raiz e atualizar o cabeçalho
int bTreeInsert(record *newRecord, btPage **root, FILE *fp){
    
    assert(fp != NULL);

    // criacao da chave
    promotedKey *key = (promotedKey*)malloc(sizeof(promotedKey));
    key->key = newRecord->key;
    key->recordRRN = newRecord->recordRRN;

    // os filhos da chave promovida inicialmente são -1
    key->childs[0] = -1;
    key->childs[1] = -1;
    
    // executa a insersao recursiva
    promotedKey *promotionKey = _bTreeInsert(*root, key, fp);

    // se houve promoção a esse ponto, deve se criar uma nova raiz
    if (promotionKey != NULL) {
        btPage *nextRoot = createNodeWithPromotedKey(promotionKey);
        freePage(*root);
        *root = setNodeAsRoot(nextRoot, fp);
    }

    // TODO return sucess
    free(key);
    free(promotionKey);
    return 0;

}
